/*!
 * OMB slideshow plugin
 */
;(function ( $, window, document, undefined ) {

    $.fn.tmdbSlideshow = function ( options ) {
        options = $.extend( {}, $.fn.tmdbSlideshow.options, options );

        return this.each(function () {

            var elm 				     = $(this),
            	elm_main_images    = elm.find(".tmdb-slideshow-main-images"),
            	elm_main_width 	   = elm_main_images.find(">li").eq(0).width(),
                elm_main_thumbs_wrapp = elm.find(".tmdb-slideshow-thumbnails-wrapper");
            	elm_main_thumbs 	 = elm.find(".tmdb-slideshow-thumbnails"),
            	elm_thumbs_width	 = 0,
            	elm_thumb_width    = 0,
            	elm_prev_image_btn = elm.find(".tmdb-slideshow-header-prev"),
            	elm_next_image_btn = elm.find(".tmdb-slideshow-header-next"),
            	elm_current 		   = 0,
            	move_to				     = 0,
            	elm_total 			   = 0,
            	thumb_current 		 = 0,
            	thumb_move_to		   = 0,
                resize_int         = 0;

            function init()
            {
            	elm_main_images.scrollLeft(0);
              	elm_main_images.find("li").eq(0).addClass( 'tmdb-focus' );

            	count_elements();
            	print_thumbs();
            	triggers();
            }

            function swipe() 
            {
                var is_press = false,
                    init_click_pos,
                    c_focused_image = $(),
                    has_to_go_right = 0,
                    has_to_go_left = 0;

                // upward magic

                elm_main_images.on('mousedown', elm_main_images.find(".tmdb-focus"), function(e) {
                    c_focused_image = elm_main_images.find(".tmdb-focus")
                    is_press = true;
                    init_click_pos = e.pageX;

                    c_focused_image.css('transition', 'none');

                    return false;
                });

                $(document).on('mouseup', elm_main_images.find(".tmdb-focus"), function() {
                    is_press = false;
                    if( c_focused_image.size() > 0 ){
                      c_focused_image.css('transition', '0.5s');
                      c_focused_image.css('left', '0px');
                    }
                    has_to_go_right = 0;
                    has_to_go_left = 0;
                });

                var dir = 'none',
                    move_after_px = 300;

                elm_main_images.mousemove(function(e) {
                    if (is_press === true) {

                        if (init_click_pos - e.pageX) {

                            c_focused_image.css('left', -(init_click_pos - e.pageX))

                            if (parseInt(c_focused_image.css('left')) < -move_after_px) {
                                has_to_go_right = has_to_go_right + 1;

                                if (has_to_go_right == 1) {
                                    move_to = move_to + 1;

                                    if (move_to >= elm_total) {
                                        move_to = 0;
                                    }

                                    elm_main_images.css({
                                        'left': "-" + (elm_main_width * move_to) + "px",
                                        'transition': 'left 350ms ease-in-out'
                                    });
                                }

                                setTimeout( function() { elm_main_images.find("li").eq(move_to).prev().removeClass('tmdb-focus'); }, 360 );
                                elm_main_images.find("li").eq(move_to).addClass('tmdb-focus');

                                elm_current = move_to;

                                count_elements();

                                thumb_move_to = Math.ceil((elm_current + 1) / options.number_thumbs) - 1;
                                move_thumbs();

                            } else if (parseInt(c_focused_image.css('left')) > move_after_px) {

                                has_to_go_left = has_to_go_left + 1;

                                if (has_to_go_left == 1) {
                                    move_to = move_to - 1;
                                    if (move_to < 0) {
                                        move_to = elm_total - 1;
                                    }
                                    elm_main_images.css({
                                        'left': "-" + (elm_main_width * move_to) + "px",
                                        'transition': 'left 350ms ease-in-out'
                                        });
                                    }

                                    setTimeout( function() { elm_main_images.find("li").eq(move_to).next().removeClass('tmdb-focus'); }, 360 );

                                    elm_main_images.find("li").eq(move_to).addClass('tmdb-focus');

                                    elm_current = move_to;

                                    count_elements();

                                    thumb_move_to = Math.ceil((elm_current + 1) / options.number_thumbs) - 1;
                                    move_thumbs();

                            }
                        }
                    }
                });

                // same thing for the thumbnails slider

                var is_press2 = false,
                    init_click_pos2;

                elm_main_thumbs_wrapp.on('mousedown', function(e) {
                    is_press2 = true;
                    init_click_pos2 = e.pageX;

                    return false;
                });

                $(document).on('mouseup', function() {
                    is_press2 = false;
                    
                    has_to_go_right = 0;
                    has_to_go_left = 0;
                });

                $(document).mousemove(function(e) {
                    if ( is_press2 === true ) {

                        if ( (init_click_pos2 - e.pageX) > 150 ) {


                            thumb_move_to = thumb_move_to + 1;
                            if( thumb_move_to >= (elm_total / options.number_thumbs) ){
                                thumb_move_to = 0;
                            }

                            move_thumbs();

                            is_press2 = false;

                        }
                        else if ( (init_click_pos2 - e.pageX) < -150 )  {

                            thumb_move_to = thumb_move_to - 1;
                            if( thumb_move_to < 0 ){
                                thumb_move_to = Math.ceil(elm_total / options.number_thumbs) -1;
                            }

                            move_thumbs();

                            is_press2 = false;
                        }
                    }
                });
            }

            function print_thumbs()
           	{
           		if( options.show_thumbs === true ){
           			elm.addClass("tmdb-slideshow-with-thumbnails");
           		}

           		elm_thumbs_width 	= elm_main_thumbs.outerWidth(),
            	elm_thumb_width 	= elm_main_thumbs.find(">li").eq(0).outerWidth();

           		var margin = ( elm_thumbs_width - ( elm_thumb_width * options.number_thumbs ) ) / ( 2 * options.number_thumbs );
           		elm_main_thumbs.find(">li").css({
           			'margin-left': ( margin ) + "px",
           			'margin-right': ( margin ) + "px"
           		});
           	}

            function count_elements()
            {
            	elm_total = elm_main_images.find(">li").size();

            	elm.find(".tmdb-slideshow-current").text( elm_current + 1 );
            	elm.find(".tmdb-slideshow-total").text( elm_total );

            	elm_main_thumbs.find("li.tmdb-on").removeClass('tmdb-on');
            	elm_main_thumbs.find(">li").eq(elm_current).addClass('tmdb-on');
            }

            function move(force_move) 
            {
                force_move = force_move ? force_move : false;

                if (typeof(options.onStart) === 'function') options.onStart();

                if (force_move !== true && elm_current == move_to) return;

                // console.log( elm_current, move_to, elm_main_width );

                elm_main_images.css({
                    'left': "-" + (elm_main_width * move_to) + "px",
                    'transition': 'left 350ms ease-in-out'
                });

                elm_main_images.find(".tmdb-focus").removeClass("tmdb-focus");
                elm_main_images.find("li").eq(move_to).addClass('tmdb-focus');

                elm_current = move_to;

                count_elements();

                thumb_move_to = Math.ceil((elm_current + 1) / options.number_thumbs) - 1;
                move_thumbs();

                if (typeof(options.onEnd) === 'function') options.onEnd();
            }

            function move_thumbs()
            {
            	if( thumb_current == thumb_move_to ) return;

            	//console.log( elm_thumbs_width * thumb_move_to );

            	elm_main_thumbs.css({
            		'left': "-" + ( elm_thumbs_width * thumb_move_to ) + "px",
            		'transition' : 'left 350ms ease-in-out'
            	});

            	thumb_current = thumb_move_to;

            	//count_elements();
            }

            function enter_thumbs_mode()
            {
              elm.addClass('tmdb-slideshow-thumbnails-mode');
              elm_main_thumbs.css('left', 0);
            }

            function exit_thumbs_mode()
            {
              elm.removeClass('tmdb-slideshow-thumbnails-mode');
            }

            function responsive_re_init()
            {
              //console.info( 'reinit' );
              elm_main_width = elm_main_images.find(">li").eq(0).width();
              print_thumbs();
              move( true );
            }

            function enter_fullscreen_mode()
            {
              var fsholder = $('<div class="tmdb-slideshow-fullscreen-holder tmdb" />');

              elm.after("<div class='tmdb-slideshow-marker' />");
              fsholder.html(elm);

              launchIntoFullscreen( fsholder.get(0) );

              $('body').append(fsholder);
              responsive_re_init();
            }

            function exit_fullscreen_mode()
            {
              $(".tmdb-slideshow-marker")
                .before( elm )
                .remove();

              responsive_re_init();
              exitFullscreen();
              $(".tmdb-slideshow-fullscreen-holder").remove();
            }

            function launchIntoFullscreen(element)
            {
              if(element.requestFullscreen) {
                element.requestFullscreen();
              } else if(element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
              } else if(element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
              } else if(element.msRequestFullscreen) {
                element.msRequestFullscreen();
              }
            }

            function exitFullscreen()
            {
              if(document.exitFullscreen) {
                document.exitFullscreen();
              } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
              } else if(document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
              }
            }

            function triggers()
           	{
              swipe();

           		elm_prev_image_btn.click( function(e){
           			e.preventDefault();

           			move_to = move_to - 1;
           			if( move_to < 0 ){
           				move_to = elm_total - 1;
           			}

           			move();
           		} );

           		elm_next_image_btn.click( function(e){
           			e.preventDefault();

           			move_to = move_to + 1;
           			if( move_to >= elm_total ){
           				move_to = 0;
           			}

           			move();
           		} );

           		elm_main_thumbs.find(">li").click( function(e){
           			e.preventDefault();
                exit_thumbs_mode();

           			move_to = $(this).index();
           			move();
           		} );

           		elm.find(".tmdb-slideshow-thumbnails-next").click( function(e){
           			e.preventDefault();

           			thumb_move_to = thumb_move_to + 1;
           			if( thumb_move_to >= (elm_total / options.number_thumbs) ){
           				thumb_move_to = 0;
           			}

           			move_thumbs();
           		});

           		elm.find(".tmdb-slideshow-thumbnails-prev").click( function(e){
           			e.preventDefault();

           			thumb_move_to = thumb_move_to - 1;
           			if( thumb_move_to < 0 ){
           				thumb_move_to = Math.ceil(elm_total / options.number_thumbs) -1;
           			}
           			move_thumbs();
           		});

              elm.find(".tmdb-slideshow-btn-thumb-mode").click( function(e){
                e.preventDefault();
                if(!elm.hasClass('tmdb-slideshow-thumbnails-mode')){
                  enter_thumbs_mode();
                } else {
                  exit_thumbs_mode();
                }
              });

              elm.find(".tmdb-slideshow-btn-fullscreen-mode").click( function(e){
                e.preventDefault();

                if( $('body .tmdb-slideshow-fullscreen-holder').size() == 0 ){
                  enter_fullscreen_mode();
                } else {
                  exit_fullscreen_mode();
                }
              });

              $(document).bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
                var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
                var event = state ? 'FullscreenOn' : 'FullscreenOff';

                if( event == 'FullscreenOff' ){
                  exit_fullscreen_mode();
                }
            });

              $(window).resize(function(){
                clearTimeout(resize_int);
                resize_int = setTimeout(function(){
                  responsive_re_init();
                }, 200);
              })
           	}

            init();
        });
    };

    $.fn.tmdbSlideshow.options = {

        show_thumbs: true,
        number_thumbs: 4,
        swipe: true,
        onStart: function ( elem, param ) {},
        onEnd: function ( elem, param ) {}
    };

})( jQuery, window, document );