/*!
 * OMB Booking Plugin
 */
;(function ( $, window, document, undefined ) {


    $.fn.ombBooking = function ( options ) {
        options = $.extend( {}, $.fn.ombBooking.options, options );

        return this.each(function () {

            var hall_elm 	= $(this),
            	hall_data	= hall_elm.data('map');
            
            function init()
            {
            	hall_elm.addClass('omb-theatre');
            	
            	generate_hall();
            	triggers();
            }

            function generate_hall()
			{
				var hall_width = hall_elm.width() - 40 - 40;
				var html = [];
				var ticket_info 	= '',
					ticket_count 	= 0,
					ticket_rows 	= [],
					ticket_seats 	= [];
				
				html.push('<ul class="omb-hall">');
				$.each(hall_data, function(key, value) {
					html.push('<li>');
					html.push(	'<ul>');
					
					var cc = 0;
					$.each(value.spaces, function(key2, value2){
						var _class = 'omb-available';
						if( value2 == 'empty-space' ) {
							value2 = '';
							_class = 'omb-slot';
						}else{
							value2 = value2.toString();
						}
						
						var items_size = value.spaces.length + 2,
							li_width = ( ( hall_width - ( items_size * 8 ) ) / items_size ),
							style = 'width:' + ( li_width ) +  'px; height:' + ( li_width ) +  'px; line-height:' + ( li_width ) +  'px';
						
						if( cc == 0 ){
							html.push(	'<li class="omb-row-name" style="' + ( style ) +  '">' + ( value['name'].toUpperCase() ) + '</li>');
						}
						
						if( value2.indexOf('#') > 0 ) {
							_value2 = value2.split('#');
							value2 = _value2[0];
							_class = 'omb-' + _value2[1];
							
							if( _value2[1] == 'reserved' ) {
								ticket_count++;
								if( $.inArray(value.name, ticket_rows) == -1 ) ticket_rows.push(value.name);
								ticket_seats.push(value.name+'/'+value2);
							}
						}
						html.push(	'<li class="' + (  _class ) + '" style="' + ( style ) +  '">' + ( value2 ) + '</li>');

						if( cc == ( items_size - 3) ){
							html.push(	'<li class="omb-row-name" style="' + ( style ) +  '">' + ( value['name'].toUpperCase() ) + '</li>');
						}
						cc++;
					});
					
					html.push(	'</ul>');
					html.push('</li>');
				});

				html.push('</ul>');
				
				hall_elm.find('.omb-theatre-map').html( html.join('\n') );
				
				ticket_info = 'Tickets ' + ticket_count;
				if( ticket_rows.length > 0 ) ticket_info += ', Row'+(ticket_rows.length > 1 ? 's' : '') + ' ' + ticket_rows.join(',');
				if( ticket_seats.length > 0 ) ticket_info += ', Seat'+(ticket_seats.length > 1 ? 's' : '') + ' ' + ticket_seats.join(', ');
				
				$('#reservation-info').html( ticket_info );
			}
			
			function reserve_seat( row, space_index )
			{
				var row_index = row.index(),
					space_index = space_index - 1; // fix
				
				if( typeof(hall_data[row_index]) != "undefined" ){
					hall_seat = hall_data[row_index]['spaces'][space_index].toString();
					
					if( hall_seat.indexOf('#reserved') <= 0 ) {
						hall_data[row_index]['spaces'][space_index] = hall_seat + '#reserved';
					}
				}
				
				generate_hall();
			}
			
			function unreserve_seat( row, space_index )
			{
				var row_index = row.index(),
					space_index = space_index - 1; // fix
				
				if( typeof(hall_data[row_index]) != "undefined" ){
					hall_data[row_index]['spaces'][space_index] = hall_data[row_index]['spaces'][space_index].replace('#reserved', '');
				}
				
				generate_hall();
			}
			
			function make_reservation( info )
			{
				// start preloader
				hall_elm.hide();
				ombPreloader( hall_elm.parent() );
					
				$.ajax(js_vars.ajaxurl, {
            		'dataType': 'json',
	            	'method': 'POST',
            		data: {
	            		'action': 'omb_pt_event_ajax_requests',
	            		'sub_action': 'make_reservation',
	            		'user_id': info.user_id,
	            		'event_id': options.event_id,
	            		'hall_data': JSON.stringify(hall_data),
	            		'extra_info': JSON.stringify(info)
	            	}
            	}).done(function( response ) {
            		if( response.status == 'valid' ) {
            			hall_elm.html('<div class="msg">' + response.html + '</div>');
            			$('#booking-close').show();
            			hall_elm.show();
            			ombPreloader_hide( hall_elm.parent() );
            		}else{
            			hall_elm.show();
            			ombPreloader_hide( hall_elm.parent() );
            			alert('Error in making reservation! Please try again later.');
            		}
            		
            	});
			}
			
            function triggers()
           	{
           		$(window).resize(function (){
           			generate_hall();
           		});
           		
           		$(hall_elm).on('click', '.omb-available', function(e){
           			e.preventDefault();
           			var that = $(this);
           			reserve_seat( that.parents('li').eq(0), that.index() );
           		});
           		
           		$(hall_elm).on('click', '.omb-reserved', function(e){
           			e.preventDefault();
           			var that = $(this);
           			unreserve_seat( that.parents('li').eq(0), that.index() );
           		});
           		
           		$(hall_elm).on('click', '#reserve-seats', function(e){
           			e.preventDefault();
           			
           			make_reservation( $(this).data('info') );
           		});
           	}

            init();
        });
    };

    $.fn.ombBooking.options = {
        onStart: function ( elem, param ) {},
        onEnd: function ( elem, param ) {},
        event_id: 0
    };

})( jQuery, window, document );