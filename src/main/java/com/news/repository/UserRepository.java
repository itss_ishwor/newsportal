package com.news.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.news.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
	
	User findByEmail(String email);
}
