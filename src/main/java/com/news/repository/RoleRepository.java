package com.news.repository;

import org.springframework.data.repository.CrudRepository;

import com.news.domain.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByname(String name);
}
