/*!
 * OMB slideshow plugin
 */
;(function ( $, window, document, undefined ) {

    $.fn.ombSlideshow = function ( options ) {
        options = $.extend( {}, $.fn.ombSlideshow.options, options );

        return this.each(function () {

            var elm              = $(this),
              elm_main_images    = elm.find(".omb-slideshow-main-images"),
              elm_main_width     = elm_main_images.find(">li").eq(0).width(),
              elm_main_thumbs    = elm.find(".omb-slideshow-thumbnails"),
              elm_thumbs_width   = 0,
              elm_thumb_width    = 0,
              elm_prev_image_btn = elm.find(".omb-slideshow-header-prev"),
              elm_next_image_btn = elm.find(".omb-slideshow-header-next"),
              elm_current        = 0,
              move_to            = 0,
              elm_total          = 0,
              thumb_current      = 0,
              thumb_move_to      = 0,
              resize_int         = 0;

            function init()
            {
              elm_main_images.scrollLeft(0);
                elm_main_images.find("li").eq(0).addClass( 'omb-focus' );

              count_elements();
              print_thumbs();
              triggers();
            }

            function swipe() 
            {
                var is_press = false,
                    init_click_pos,
                    c_focused_image = $(),
                    has_to_go_right = 0,
                    has_to_go_left = 0;

                elm_main_images.on('mousedown', elm_main_images.find(".omb-focus"), function(e) {
                    c_focused_image = elm_main_images.find(".omb-focus")
                    is_press = true;
                    init_click_pos = e.pageX;

                    c_focused_image.css('transition', 'none');

                    return false;
                });

                $(document).on('mouseup', elm_main_images.find(".omb-focus"), function() {
                    is_press = false;
                    if( c_focused_image.size() > 0 ){
                      c_focused_image.css('transition', '0.5s');
                      c_focused_image.css('left', '0px');
                    }
                    has_to_go_right = 0;
                    has_to_go_left = 0;
                });

                var dir = 'none',
                    move_after_px = 300;

                elm_main_images.mousemove(function(e) {
                    if (is_press === true) {

                        if (init_click_pos - e.pageX) {

                            c_focused_image.css('left', -(init_click_pos - e.pageX))

                            if (parseInt(c_focused_image.css('left')) < -move_after_px) {
                                has_to_go_right = has_to_go_right + 1;

                                if (has_to_go_right == 1) {
                                    move_to = move_to + 1;

                                    if (move_to >= elm_total) {
                                        move_to = 0;
                                    }

                                    elm_main_images.css({
                                        'left': "-" + (elm_main_width * move_to) + "px",
                                        'transition': 'left 350ms ease-in-out'
                                    });
                                }

                                setTimeout( function() { elm_main_images.find("li").eq(move_to).prev().removeClass('omb-focus'); }, 360 );
                                elm_main_images.find("li").eq(move_to).addClass('omb-focus');

                                elm_current = move_to;

                                count_elements();

                                thumb_move_to = Math.ceil((elm_current + 1) / options.number_thumbs) - 1;
                                move_thumbs();

                            } else if (parseInt(c_focused_image.css('left')) > move_after_px) {

                                has_to_go_left = has_to_go_left + 1;

                                if (has_to_go_left == 1) {
                                    move_to = move_to - 1;
                                    if (move_to < 0) {
                                        move_to = elm_total - 1;
                                    }
                                    elm_main_images.css({
                                        'left': "-" + (elm_main_width * move_to) + "px",
                                        'transition': 'left 350ms ease-in-out'
                                        });
                                    }

                                    setTimeout( function() { elm_main_images.find("li").eq(move_to).next().removeClass('omb-focus'); }, 360 );

                                    elm_main_images.find("li").eq(move_to).addClass('omb-focus');

                                    elm_current = move_to;

                                    count_elements();

                                    thumb_move_to = Math.ceil((elm_current + 1) / options.number_thumbs) - 1;
                                    move_thumbs();

                            }
                        }
                    }
                });
            }

            function print_thumbs()
            {
              if( options.show_thumbs === true ){
                elm.addClass("omb-slideshow-with-thumbnails");
              }

              elm_thumbs_width  = elm_main_thumbs.outerWidth(),
              elm_thumb_width   = elm_main_thumbs.find(">li").eq(0).outerWidth();

              var margin = ( elm_thumbs_width - ( elm_thumb_width * options.number_thumbs ) ) / ( 2 * options.number_thumbs );
              elm_main_thumbs.find(">li").css({
                'margin-left': ( margin ) + "px",
                'margin-right': ( margin ) + "px"
              });
            }

            function count_elements()
            {
              elm_total = elm_main_images.find(">li").size();

              elm.find(".omb-slideshow-current").text( elm_current + 1 );
              elm.find(".omb-slideshow-total").text( elm_total );

              elm_main_thumbs.find("li.omb-on").removeClass('omb-on');
              elm_main_thumbs.find(">li").eq(elm_current).addClass('omb-on');
            }

            function move(force_move) 
            {
                force_move = force_move ? force_move : false;

                if (typeof(options.onStart) === 'function') options.onStart();

                if (force_move !== true && elm_current == move_to) return;

                // console.log( elm_current, move_to, elm_main_width );

                elm_main_images.css({
                    'left': "-" + (elm_main_width * move_to) + "px",
                    'transition': 'left 350ms ease-in-out'
                });

                elm_main_images.find(".omb-focus").removeClass("omb-focus");
                elm_main_images.find("li").eq(move_to).addClass('omb-focus');

                elm_current = move_to;

                count_elements();

                thumb_move_to = Math.ceil((elm_current + 1) / options.number_thumbs) - 1;
                move_thumbs();

                if (typeof(options.onEnd) === 'function') options.onEnd();
            }

            function move_thumbs()
            {
              if( thumb_current == thumb_move_to ) return;

              //console.log( elm_thumbs_width * thumb_move_to );

              elm_main_thumbs.css({
                'left': "-" + ( elm_thumbs_width * thumb_move_to ) + "px",
                'transition' : 'left 350ms ease-in-out'
              });

              thumb_current = thumb_move_to;

              //count_elements();
            }

            function enter_thumbs_mode()
            {
              elm.addClass('omb-slideshow-thumbnails-mode');
              elm_main_thumbs.css('left', 0);
            }

            function exit_thumbs_mode()
            {
              elm.removeClass('omb-slideshow-thumbnails-mode');
            }

            function responsive_re_init()
            {
              //console.info( 'reinit' );
              elm_main_width = elm_main_images.find(">li").eq(0).width();
              print_thumbs();
              move( true );
            }

            function enter_fullscreen_mode()
            {
              var fsholder = $('<div class="omb-slideshow-fullscreen-holder omb" />');

              elm.after("<div class='omb-slideshow-marker' />");
              fsholder.html(elm);

              launchIntoFullscreen( fsholder.get(0) );

              $('body').append(fsholder);
              responsive_re_init();
            }

            function exit_fullscreen_mode()
            {
              $(".omb-slideshow-marker")
                .before( elm )
                .remove();

              responsive_re_init();
              exitFullscreen();
              $(".omb-slideshow-fullscreen-holder").remove();
            }

            function launchIntoFullscreen(element)
            {
              if(element.requestFullscreen) {
                element.requestFullscreen();
              } else if(element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
              } else if(element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
              } else if(element.msRequestFullscreen) {
                element.msRequestFullscreen();
              }
            }

            function exitFullscreen()
            {
              if(document.exitFullscreen) {
                document.exitFullscreen();
              } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
              } else if(document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
              }
            }

            function triggers()
            {
              swipe();

              elm_prev_image_btn.click( function(e){
                e.preventDefault();

                move_to = move_to - 1;
                if( move_to < 0 ){
                  move_to = elm_total - 1;
                }

                move();
              } );

              elm_next_image_btn.click( function(e){
                e.preventDefault();

                move_to = move_to + 1;
                if( move_to >= elm_total ){
                  move_to = 0;
                }

                move();
              } );

              elm_main_thumbs.find(">li").click( function(e){
                e.preventDefault();
                exit_thumbs_mode();

                move_to = $(this).index();
                move();
              } );

              elm.find(".omb-slideshow-thumbnails-next").click( function(e){
                e.preventDefault();

                thumb_move_to = thumb_move_to + 1;
                if( thumb_move_to >= (elm_total / options.number_thumbs) ){
                  thumb_move_to = 0;
                }

                move_thumbs();
              });

              elm.find(".omb-slideshow-thumbnails-prev").click( function(e){
                e.preventDefault();

                thumb_move_to = thumb_move_to - 1;
                if( thumb_move_to < 0 ){
                  thumb_move_to = Math.ceil(elm_total / options.number_thumbs) -1;
                }
                move_thumbs();
              });

              elm.find(".omb-slideshow-btn-thumb-mode").click( function(e){
                e.preventDefault();
                if(!elm.hasClass('omb-slideshow-thumbnails-mode')){
                  enter_thumbs_mode();
                } else {
                  exit_thumbs_mode();
                }
              });

              elm.find(".omb-slideshow-btn-fullscreen-mode").click( function(e){
                e.preventDefault();

                if( $('body .omb-slideshow-fullscreen-holder').size() == 0 ){
                  enter_fullscreen_mode();
                  $('.omb-slideshow-enter-fullscreen').hide();
                  $('.omb-slideshow-exit-fullscreen').show();
                } else {
                  exit_fullscreen_mode();
                  $('.omb-slideshow-enter-fullscreen').show();
                  $('.omb-slideshow-exit-fullscreen').hide();
                }
              });

              $(document).bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
                var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
                var event = state ? 'FullscreenOn' : 'FullscreenOff';

                if( event == 'FullscreenOff' ){
                  exit_fullscreen_mode();
                }
            });

              $(window).resize(function(){
                clearTimeout(resize_int);
                resize_int = setTimeout(function(){
                  responsive_re_init();
                }, 200);
              })
            }

            init();
        });
    };

    $.fn.ombSlideshow.options = {

        show_thumbs: true,
        number_thumbs: 4,
        swipe: true,
        onStart: function ( elem, param ) {},
        onEnd: function ( elem, param ) {}
    };

})( jQuery, window, document );